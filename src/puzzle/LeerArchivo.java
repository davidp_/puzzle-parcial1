package puzzle;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class LeerArchivo {
    private String ruta;
    ArrayList array;
    public LeerArchivo(String ruta) {
        this.ruta = ruta;
        array = new ArrayList();
    }
    
    public ArrayList getData() throws FileNotFoundException, IOException {
        String cadena;
        String data = "";
        FileReader fr = new FileReader(ruta);
        BufferedReader br = new BufferedReader(fr);
        while((cadena = br.readLine()) != null) {
            data = cadena;
            cleanData(data);
        }        
        br.close();
        //System.out.println("array: "+array);
        return array;
    }
    
    public void cleanData(String data) {
        String[] temp = data.split(",");
        for(String item: temp) {
            array.add(item);
        }
    }
}
