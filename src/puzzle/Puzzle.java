package puzzle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Puzzle {

    public static void main(String[] args) {
        LeerArchivo la = new LeerArchivo("D:\\Davidp_\\Documents\\NetBeansProjects\\Puzzle\\src\\init\\prueba.txt");
        try {
            ArrayList data = la.getData();
            System.out.println("data: "+data);
            Tablero t = new Tablero(data);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    } 
}
